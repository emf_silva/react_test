import React, { Component } from 'react';

class RepoInformation extends Component {
	
	constructor(props){
		super(props);
		this.state = {
			 login: props.user.login,
			 repos: []
		};
		this.getRepositoryInformation();
	}
		
		
	getRepositoryInformation() {
		fetch('https://api.github.com/users/' + this.state.login + '/repos?type=all')
		.then((response) => response.json())
		.then((json) => this.setState({repos: json}))
	}
		
	render(){	
		return(
			<div>		
				<h2>Repository Data </h2>
				<br/>
				{this.state.repos.map((repo,index) => 
					<p key={index}><b>Name:</b> <a href={repo.html_url}>{repo.name}</a> <b>Description:</b> {repo.description}</p>)}
			</div>
		);
	}
}

export default RepoInformation;

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import UserInformation from './UserInformation';
import RepoInformation from './RepoInformation';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = { user: {login: "",
					n: "",
					location: "",
					avatarPath: ""},
			 		hideButton: false};
  }

  getUserInformation() {

	fetch('https://api.github.com/users/gaearon')
	.then((response) => response.json())
	.then((json) => this.setState( {user: {
				login: json.login,
				n: json.name,
				location: json.location,
				avatarPath: json.avatar_url
			}, hideButton: true }))
  }

  render() {
	const hideButton = this.state.hideButton;

	const toggleButton = !hideButton ? (
		<div className="App-intro"> 
		  <hr/>
		  <p>Click on the button to fetch the user information</p>
		  <button onClick={this.getUserInformation.bind(this)}>
		    Click me
          </button>
        </div>
	) : null;

	const toogleUserInformation = hideButton ? <UserInformation {...this.state}/> : null;

	const toogleRepoInformation = hideButton ? <RepoInformation {...this.state}/> : null;

    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
		{toggleButton}
        {toogleUserInformation}
		<br/>
		{toogleRepoInformation}
      </div>
    );
  }
}

export default App;

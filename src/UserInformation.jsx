import React from 'react';

const UserInformation = props => (
  <div>
	<h2>User Data</h2>
	<br/>
	<div><img src={props.user.avatarPath} alt='Avatar' /></div>
	<br/>	
	<div>Username: {props.user.login}</div>
	<div>Name: {props.user.n}</div>
	<div>Location: {props.user.location}</div>
  </div>
);

export default UserInformation;
